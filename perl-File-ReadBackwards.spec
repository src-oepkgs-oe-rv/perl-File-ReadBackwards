Name:           perl-File-ReadBackwards
Version:        1.05
Release:        1
Summary:        Read a file backwards by lines
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            https://metacpan.org/release/File-ReadBackwards
Source0:        https://cpan.metacpan.org/authors/id/U/UR/URI/File-ReadBackwards-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  perl-generators
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(Test::More) >= 0.44
BuildRequires:  perl(File::Temp)
BuildRequires:  perl(strict) perl(vars)
BuildRequires:  perl(Carp) perl(Config) perl(Fcntl) perl(Symbol)
Requires:       perl(:MODULE_COMPAT_%(eval "`perl -V:version`"; echo $version))

%description
This module reads a file backwards line by line. It is simple to use,
memory efficient and fast. It supports both an object and a tied handle
interface.

%package_help

%prep
%autosetup -n File-ReadBackwards-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1
%make_build

%install
%make_install
%{_fixperms} %{buildroot}/*

%check
make test

%files
%defattr(-,root,root)
%doc README
%{perl_vendorlib}/*

%files help
%defattr(-,root,root)
%doc Changes
%{_mandir}/man3/*

%changelog
* Mon Aug 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.05-1
- Package Init

